"use strict";

let cinemaArray = new Array(4);
for (let i = 0; i < cinemaArray.length; i++) {
  cinemaArray[i] = new Array(5);
  for (let j = 0; j < cinemaArray[i].length; j++) {
    cinemaArray[i][j] = "[" + i + "," + j + "]";
  }
}
let cashBox = 0;

// Продажа билетов: ряд, место, имя, тел
function purchase(a, b, c, d) {
  if (typeof cinemaArray[a - 1][b - 1] !== "object") {
    cinemaArray[a - 1][b - 1] = { name: [c], tel: [d] };
    if (a === 1) {
      cashBox += 50;
    } else if (a === cinemaArray.length) {
      cashBox += 200;
    } else {
      cashBox += 100;
    }
  } else {
    console.log(`Sorry, the places are busy`);
  }
}

//касса
function cash() {
  console.log(`cash box = ${cashBox} USD`);
}

//Возврат
function ticketRefund(a, b) {
  cinemaArray[a - 1][b - 1] = `[${a - 1},${b - 1}]`;
  if (a === 1) {
    cashBox -= 25;
  } else if (a === cinemaArray.length) {
    cashBox -= 100;
  } else {
    cashBox -= 50;
  }
}

//заполненость
function Places() {
  for (let i = 0; i < cinemaArray.length; ++i) {
    let fullnessPlace = cinemaArray[i].map(function (item) {
      if (typeof item == "object") {
        item = `[*]`;
      } else {
        item = `[ ]`;
      }
      return item;
    });
    console.log(fullnessPlace.join(" "));
  }
}

//список
function list() {
  for (let i = 0; i < cinemaArray.length; ++i) {
    cinemaArray[i].forEach(function (element, index) {
      if (i === 0) {
        cashBox = 50;
      } else if (i === cinemaArray.length - 1) {
        cashBox = 200;
      } else {
        cashBox = 100;
      }

      if (typeof element !== "object") {
        console.log(`${i + 1} row ${index + 1} place    Еmpty`);
      } else {
        console.log(
          `${i + 1} row ${index + 1} place    Name  ${
            element.name
          }    cash ${cashBox} USD`
        );
      }
    });
  }
}

//
//
//
//
//
//

purchase(1, 1, "Vadim", "0979196076"); // Продажа
purchase(2, 2, "Vadim", "0979196076"); // Продажа
purchase(3, 3, "Vadim", "0979196076"); // Продажа
purchase(4, 4, "Vadim", "0979196076"); // Продажа
purchase(4, 5, "Vadim", "0979196076"); // Продажа
purchase(2, 3, "Vadim", "0979196076"); // Продажа
ticketRefund(4, 4); // Возврат

cash(); // Касса
console.log("\n");
Places(); // Заполненость
console.log("\n");
list(); // Список
